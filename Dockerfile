FROM selenium/node-firefox
USER root
WORKDIR /app
RUN mkdir result_share && chown 1000 result_share
RUN mkdir agent_share && chown 1000 agent_share
COPY requirements.txt .
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3.10 python3-pip\
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
USER 1000
RUN pip install --no-cache-dir --upgrade -r requirements.txt
COPY . .

ENTRYPOINT [ "python3" ]