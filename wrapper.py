import logging
logging.error("testest")
import os.path
import pathlib
import time
import traceback
from multiprocessing import Process

import uvicorn as uvicorn
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import yaml
import signal
import requests
import wrapper_logger

try:
    import main

    with open('config.yaml') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
        print(type(config))
except ImportError:
    pass

app = FastAPI()


class Job(BaseModel):
    job_id: str
    agent: dict
    data: dict


@app.get("/healthy")
def healthy():
    return {'msg': "im good"}


@app.post("/run")
async def run(job: Job):
    for required in config.get('requirements'):
        if not job.data.get(required['name']) and not required['optional']:
            raise HTTPException(status_code=400, detail="missing required parameter")
    Process(target=start, args=(job,)).start()


def extract_selectors(job: Job) -> list:
    selectors = []
    for required in config.get('requirements'):
        if required.get('selector') and job.data.get(required['name']):
            selectors.extend(job.data.get(required['name']) if type(job.data.get(required['name'])) == list else [
                job.data.get(required['name'])])
    return selectors


def set_timeout(job: Job) -> None:
    try:
        def handler(signum, frame):
            raise TimeoutError

        signal.signal(signal.SIGALRM, handler)
        if config.get('timeoutpr'):
            timeout = int(config.get('timeout')) * 60 * len(job.data[config.get('timeoutpr')])
        else:
            timeout = int(config.get('timeout')) * 60
        signal.alarm(timeout)
    except AttributeError:
        print("Windows does not support signal SIGLRM")


def start(job: Job):
    error = False
    result = False
    selectors = extract_selectors(job)
    logger = wrapper_logger.get_logger(config.get('module'), job.job_id, selectors=selectors)
    logger.info(f"starting job with parameters: {job.data}")

    try:

        output_path = os.path.join('result_share', job.job_id, config.get('module'))
        pathlib.Path(output_path).mkdir(parents=True, exist_ok=True)
        set_timeout(job)
        result = main.run(job.data, job.agent, output_path, logger)
    except TimeoutError:
        logger.error("timeout")
        error = True
    except:
        logger.error(f"error happened while running scraper:\n{traceback.format_exc()}")
        error = True
    finally:
        logger.handlers[0].close()
        try:
            signal.alarm(0)
        except AttributeError:
            pass

    response = {
        "job_id": job.job_id,
        "status": "failed" if error else "finished",
        "result": result,
        "agent_id": job.agent.get("agent_id")
    }

    requests.get(f"http://{os.environ['job_manager_host']}:5001/job_done", json=response)


def broadcast_config():
    while 1:
        try:
            res = requests.get(f"http://{os.environ['job_manager_host']}:5001/im_alive",
                               json={"module": config.get("module"), "config": config})
            if res.status_code == 200:
                break
        except requests.exceptions.ConnectionError:
            logging.error(traceback.format_exc())
            time.sleep(5)


if __name__ == '__main__':
    logging.info("broadcast config")
    broadcast_config()
    logging.info("starting service")
    uvicorn.run("wrapper:app", host="0.0.0.0", port=8001)
