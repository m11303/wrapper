import datetime
import logging
import traceback
from typing import List

from sqlalchemy.orm import Session
import job_db


class SQLAlchemyHandler(logging.Handler):

    def __init__(self, db_session: Session, module: str, job_id: str, selectors: List[str] = None):
        self.db_session = db_session
        self.module = module
        self.job_id = job_id
        self.selectors = selectors
        super().__init__()

    def _sanitize(self, text: str) -> str:
        if text and self.selectors:
            for selector in self.selectors:
                text = str(text).replace(selector, "SELECTOR")
        return text

    def emit(self, record):
        trace = None
        exc = record.__dict__['exc_info']
        if exc:
            trace = traceback.format_exc()
        log = job_db.Log(
            module=self.module,
            job_id=self.job_id,
            timestamp=datetime.datetime.now(),
            logger=record.__dict__['name'],
            level=record.__dict__['levelname'],
            trace=self._sanitize(trace),
            msg=self._sanitize(record.__dict__['msg']), )
        self.db_session.add(log)
        self.db_session.commit()


def get_logger(module: str, job_id: str, selectors: List[str] = None) -> logging.Logger:
    db_session = Session(job_db.engine)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(SQLAlchemyHandler(db_session=db_session, job_id=job_id, module=module, selectors=selectors))
    return logger
